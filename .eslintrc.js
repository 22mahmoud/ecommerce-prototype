module.exports = {
  extends: ['airbnb-base'],
  rules: {
    'no-underscore-dangle': 0,
    'import/prefer-default-export': 0,
    'arrow-parens': 0,
    'linebreak-style': 0,
  },
};
