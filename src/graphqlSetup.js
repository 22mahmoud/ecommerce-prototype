import { writeFileSync } from 'fs';
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas';
import { makeExecutableSchema } from 'apollo-server';

const typeDefs = mergeTypes(fileLoader(`${__dirname}/modules/**/*.schema.js`), {
  all: true,
});

const resolvers = mergeResolvers(
  fileLoader(`${__dirname}/modules/**/*.resolvers.js`),
  { all: true },
);

writeFileSync('schema.graphql', typeDefs);

export const schema = makeExecutableSchema({ resolvers, typeDefs });
