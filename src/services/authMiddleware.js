import { decodeToken } from '../modules/user/auth';

export const jwtCheck = authorization => {
  const token = authorization && authorization.replace('Bearer ', '');
  try {
    const user = decodeToken(token);
    return user;
  } catch (error) {
    throw error;
  }
};
