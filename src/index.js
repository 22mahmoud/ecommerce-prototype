import '@babel/polyfill';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';

import dbInit from './config/db';
import { jwtCheck } from './services/authMiddleware';
import { schema } from './graphqlSetup';
import constants from './config/constants';

dbInit();

const app = express();

app.use((req, _, next) => {
  try {
    const { authorization } = req.headers;
    const user = jwtCheck(authorization);
    req.user = user;
    return next();
  } catch (error) {
    return next();
  }
});

const server = new ApolloServer({
  schema,
  context: ({ req }) => ({ user: req.user }),
});

const cors = {
  credentials: true,
  origin: '*',
};

server.applyMiddleware({ app, path: '/graphql', cors });

app.listen({ port: constants.PORT }, () => {
  // eslint-disable-next-line no-console
  console.log(
    `🚀 Server ready at http://localhost:${constants.PORT}${
      server.graphqlPath
    }`,
  );
});
