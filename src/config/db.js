/* eslint-disable no-console */
import mongoose from 'mongoose';
import constants from './constants';

export default () => {
  if (process.env.NODE_ENV === 'develpoment') {
    mongoose.set('debug', true);
  }

  try {
    mongoose.connect(constants.MONGO_URL);
  } catch (err) {
    mongoose.createConnection(constants.MONGO_URL);
  }

  mongoose.connection
    .on('open', () => console.log('MongoDB Running'))
    .on('error', err => {
      throw err;
    });
};
