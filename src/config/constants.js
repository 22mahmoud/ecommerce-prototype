require('dotenv').config();

const devCofing = {
  MONGO_URL: process.env.MONGO_URL_DEV,
  SALT_ROUND: 8,
};

const testConfig = {
  MONGO_URL: process.env.MONGO_URL_TEST,
  SALT_ROUND: 4,
};

const prodConfig = {
  MONGO_URL: process.env.MONGO_URL_PROD,
  SALT_ROUND: 10,
};

const defaultConfig = {
  PORT: process.env.PORT || 4000,
  JWT_SECRET: process.env.JWT_SECRET,
  GRAPHQL_PATH: '/graphql',
};

function envConfig(env) {
  switch (env) {
    case 'develpoment':
      return devCofing;
    case 'test':
      return testConfig;
    default:
      return prodConfig;
  }
}

export default {
  ...defaultConfig,
  ...envConfig(process.env.NODE_ENV),
};
