import mongoose from 'mongoose';

export const isValidMongoObjectID = id => mongoose.Types.ObjectId.isValid(id);
