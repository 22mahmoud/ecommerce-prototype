import { AuthenticationError } from 'apollo-server-express';
import { userService } from '../modules';

export function requireAuth(fn) {
  return async (root, args, context, info) => {
    if (!context.user || !context.user._id) {
      return new AuthenticationError('You must be signed in to do this');
    }

    const user = await userService.getUserById(context.user._id);

    if (!user) {
      return new AuthenticationError('You must be signed in to do this');
    }

    return fn(root, args, context, info);
  };
}
