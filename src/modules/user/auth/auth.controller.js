import jwt from 'jsonwebtoken';
import constants from '../../../config/constants';

// eslint-disable-next-line max-len
export const createToken = user => jwt.sign({ _id: user._id, email: user.email }, constants.JWT_SECRET);

export const decodeToken = token => jwt.verify(token, constants.JWT_SECRET);
