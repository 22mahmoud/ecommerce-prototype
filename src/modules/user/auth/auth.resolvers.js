import { createToken } from './auth.controller';
import { userService } from '..';

export default {
  Mutation: {
    async signup(_, args) {
      try {
        const user = await userService.createUser(args);
        return {
          token: createToken(user),
        };
      } catch (error) {
        throw error;
      }
    },

    async login(_, { email, password }) {
      try {
        const user = await userService.getUserByEmail(email);
        const isValidPassword = user.comparePassword(password);
        if (!isValidPassword) {
          throw Error('incorrect password');
        }

        return {
          token: createToken(user),
        };
      } catch (error) {
        throw error;
      }
    },
  },
};
