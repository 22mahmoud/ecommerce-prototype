import { gql } from 'apollo-server-express';

export default gql`
  type AuthRespose {
    token: String!
  }

  type Mutation {
    signup(email: String!, password: String!): AuthRespose!
    login(email: String!, password: String!): AuthRespose!
  }
`;
