import * as Yup from 'yup';

import { UserModel } from './user.model';
import { isValidMongoObjectID } from '../../utils/isValidMongoObjectId';

export const createUser = async info => {
  const schema = Yup.object().shape({
    email: Yup.string()
      .email()
      .required(),
    password: Yup.string()
      .min(8)
      .required(),
  });

  try {
    await schema.validate(info);
    return UserModel.create(info);
  } catch (error) {
    throw error;
  }
};

export const getUserByEmail = async email => {
  const schema = Yup.object().shape({
    email: Yup.string()
      .email()
      .required(),
  });

  try {
    await schema.validate({ email });

    const user = await UserModel.findOne({ email });
    if (!user) {
      throw Error('there is no user registered with that email address');
    }

    return user;
  } catch (error) {
    throw error;
  }
};

export const getUserById = async id => {
  const schema = Yup.object().shape({
    id: Yup.mixed()
      .test('ownerId', 'owner is not ObjectID', v => isValidMongoObjectID(v))
      .required(),
  });

  try {
    await schema.validate({ id });

    const user = await UserModel.findById(id);

    if (!user) {
      throw Error("user doesn't exist");
    }

    return user;
  } catch (error) {
    throw error;
  }
};
