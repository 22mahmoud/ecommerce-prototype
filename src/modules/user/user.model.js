import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import uniqueValidator from 'mongoose-unique-validator';

import constants from '../../config/constants';

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      minlength: [8, 'password must be at least 8 characters'],
    },
  },
  { timestamps: true },
);

userSchema.plugin(uniqueValidator, {
  message: '{PATH}: {VALUE} is already taken.',
});

// eslint-disable-next-line func-names
userSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = this.hashPassword(this.password);
  }
  next();
});

userSchema.methods = {
  hashPassword(password) {
    return bcrypt.hashSync(password, constants.SALT_ROUND);
  },

  comparePassword(password) {
    return bcrypt.compareSync(password, this.password);
  },
};

export const UserModel = mongoose.model('User', userSchema);
