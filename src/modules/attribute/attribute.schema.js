import { gql } from 'apollo-server-express';

export default gql`
  scalar JSON

  type Attribute {
    name: String!
    type: String!
    value: JSON!
  }

  input AttributeInput {
    name: String!
    type: String!
    value: JSON!
  }

  input ValueInput {
    type: String
    value: JSON
  }

  type Mutation {
    createAttribute(attribute: AttributeInput!): Attribute!
    updateAttributeName(id: ID!, name: String): Attribute!
    updateAttributeValue(
      attrID: ID!
      valueID: ID!
      value: ValueInput
    ): Attribute!
    deleteAttribute(id: ID!): Boolean!
  }
`;
