import GraphQLJSON from 'graphql-type-json';
import { attributeService } from '.';
import { requireAuth } from '../../utils/permissions';

export default {
  JSON: GraphQLJSON,
  Mutation: {
    createAttribute: requireAuth(async (_, { type, name, value }) => {
      try {
        const attribute = await attributeService.insertAttribute({
          type,
          name,
          value,
        });
        return attribute;
      } catch (error) {
        throw error;
      }
    }),

    updateAttributeName: requireAuth(async (_, { id, name }) => {
      try {
        await attributeService.findAttributeById(id);
        const updatedAttribute = await attributeService.updateAttributeNameById(
          id,
          name,
        );
        return updatedAttribute;
      } catch (error) {
        throw error;
      }
    }),

    updateAttributeValue: requireAuth(async (_, { attrID, valueID, value }) => {
      const attribute = await attributeService.findAttributeById(attrID);
      if (String(attribute.value._id) !== String(valueID)) {
        throw new Error('wrong');
      }

      let updatedValue;

      const attrValue = await attributeService.findAttributeValueByID(valueID);

      if (attrValue.__type === value.type) {
        updatedValue = await attributeService.updateAttributeValueById({
          id: valueID,
          type: value.type,
          value: value.value,
        });

        attribute.value = updatedValue;
      } else {
        await attributeService.deleteAttributeValueById(valueID);
        updatedValue = await attributeService.insertAttributeValue({
          type: value.type,
          value: value.value,
        });
        attribute.value = updatedValue;
      }

      await attribute.save();
      return {
        name: attribute.name,
        type: attribute.value.__type,
        value: attribute.value.value,
      };
    }),

    deleteAttribute: requireAuth(async (_, { id }) => {
      try {
        await attributeService.deleteAttributeById(id);
        return true;
      } catch (error) {
        throw error;
      }
    }),
  },
};
