import mongoose from 'mongoose';

export const BaseValueModel = mongoose.model(
  'BaseValueModel',
  new mongoose.Schema(
    {},
    {
      discriminatorKey: '__type',
    },
  ),
);

export const TextValueModel = BaseValueModel.discriminator(
  'TEXT',
  new mongoose.Schema({
    value: String,
  }),
);

export const NumberValueModel = BaseValueModel.discriminator(
  'NUMBER',
  new mongoose.Schema({
    value: Number,
  }),
);

export const EnumValueModel = BaseValueModel.discriminator(
  'ENUM',
  new mongoose.Schema({
    value: [
      {
        key: {
          type: Number,
          required: true,
        },
        label: {
          type: String,
          required: true,
        },
      },
    ],
  }),
);

const setValueSchema = new mongoose.Schema({
  value: {
    type: [mongoose.Schema.Types.Mixed],
    validate: {
      validator(v) {
        return new Set([...v]).size === v.length;
      },
      message: ({ value }) => `${value} is not a valid set`,
    },
  },
});

export const SetValueModel = BaseValueModel.discriminator(
  'SET',
  setValueSchema,
);

export const NestedValueModel = BaseValueModel.discriminator(
  'NESTED',
  new mongoose.Schema({
    value: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'BaseValueModel',
    },
  }),
);

const attributeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  value: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'BaseValueModel',
  },
});

export const AttributeModel = mongoose.model('Attribute', attributeSchema);
