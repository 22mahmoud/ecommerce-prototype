import * as Yup from 'yup';
import {
  AttributeModel,
  TextValueModel,
  EnumValueModel,
  NestedValueModel,
  SetValueModel,
  BaseValueModel,
  NumberValueModel,
} from './attribute.model';
import { isValidMongoObjectID } from '../../utils/isValidMongoObjectId';

const valueSchema = Yup.mixed()
  .test('is-valid-value', 'invalid value', async function validateValue(
    _value,
  ) {
    const { type: _type } = this.parent;
    if (_type === 'SET') return new Set([..._value]).size === _value.length;
    if (_type === 'NESTED') {
      try {
        await Yup.mixed()
          .test('AttributeID', 'Attribute_id is not ObjectID', v => isValidMongoObjectID(v))
          .validate(_value);
        return true;
      } catch (e) {
        return false;
      }
    }
    if (_type === 'TEXT') return typeof _value === 'string';
    if (_type === 'NUMBER') return typeof _value === 'number';
    if (_type === 'ENUM') {
      try {
        await Yup.array()
          .of(
            Yup.object().shape({
              key: Yup.number().required(),
              label: Yup.mixed().required(),
            }),
          )
          .validate(_value);
        return true;
      } catch (e) {
        return false;
      }
    }

    return false;
  })
  .required();

const attribueSchema = Yup.object().shape({
  name: Yup.string().required(),
  type: Yup.string().oneOf(['TEXT', 'ENUM', 'NUMBER', 'NESTED', 'SET']),
  value: valueSchema,
});

export const insertAttributeValue = async ({ type, value }) => {
  const schema = Yup.object().shape({
    type: Yup.string()
      .oneOf(['TEXT', 'ENUM', 'NUMBER', 'NESTED', 'SET'])
      .required(),
    value: valueSchema,
  });

  try {
    await schema.validate({ type, value });
    let attrValue;

    if (type === 'TEXT') {
      attrValue = await TextValueModel.create({
        value,
      });
    }

    if (type === 'ENUM') {
      attrValue = await EnumValueModel.create({
        value,
      });
    }

    if (type === 'NUMBER') {
      attrValue = await NumberValueModel.create({
        value,
      });
    }

    if (type === 'NESTED') {
      attrValue = await NestedValueModel.create({
        value,
      });
    }

    if (type === 'SET') {
      attrValue = await SetValueModel.create({
        value,
      });
    }

    return attrValue;
  } catch (error) {
    throw error;
  }
};

export const insertAttribute = async info => {
  try {
    await attribueSchema.validate(info);
    const { type, value, name } = info;
    const attrValue = await insertAttributeValue({ type, value });
    const attribute = await AttributeModel.create({ name, value: attrValue });

    return attribute;
  } catch (error) {
    throw error;
  }
};

export const insertManyAttributes = async attributes => {
  const schema = Yup.array().of(attribueSchema);

  try {
    await schema.validate(attributes);

    const promises = [];

    attributes.forEach(({ type, name, value }) => {
      const promise = new Promise(async resolve => {
        const p1 = await insertAttributeValue({ type, value });
        resolve(
          AttributeModel.create({
            name,
            value: p1,
          }),
        );
      });
      promises.push(promise);
    });

    const _attributes = await Promise.all(promises);
    return _attributes;
  } catch (error) {
    throw error;
  }
};

export const findManyAttributesById = async ids => {
  const schema = Yup.array().of(
    Yup.mixed().test('AttributeID', 'Attribute_id is not ObjectID', v => isValidMongoObjectID(v)),
  );

  try {
    await schema.validate(ids);
    const attribtes = await AttributeModel.find({
      _id: {
        $in: ids,
      },
    }).populate('value');
    return attribtes;
  } catch (error) {
    throw error;
  }
};

export const findAttributeById = async id => {
  const schema = Yup.mixed()
    .test('AttributeID', 'Attribute_id is not ObjectID', v => isValidMongoObjectID(v))
    .required();
  try {
    await schema.validate(id);
    const attribute = await AttributeModel.findById(id).populate('value');

    if (!attribute) {
      throw new Error('attribute not found');
    }
    return attribute;
  } catch (error) {
    throw error;
  }
};

export const updateAttributeNameById = async (id, name) => {
  const schema = await Yup.object().shape({
    id: Yup.mixed()
      .test('AttributeID', 'Attribute_id is not ObjectID', v => isValidMongoObjectID(v))
      .required(),
    name: Yup.string(),
  });
  try {
    await schema.validate({ id, name });
    const attribute = await AttributeModel.findByIdAndUpdate(
      id,
      { name },
      { new: true },
    );
    return attribute;
  } catch (error) {
    throw error;
  }
};

export const findAttributeValueByID = async id => {
  try {
    const value = await BaseValueModel.findById(id);
    if (!value) {
      throw new Error('Value not found');
    }
    return value;
  } catch (error) {
    throw error;
  }
};

export const updateAttributeValueById = async ({ id, value, type }) => {
  const schema = Yup.object().shape({
    id: Yup.mixed()
      .test('valueID', 'value_id is not ObjectID', v => isValidMongoObjectID(v))
      .required(),
    type: Yup.string()
      .oneOf(['TEXT', 'ENUM', 'NUMBER', 'NESTED', 'SET'])
      .required(),
    value: valueSchema,
  });
  try {
    await schema.validate({ id, value, type });
    const attrValue = await findAttributeValueByID(id);

    if (type === 'TEXT' || type === 'NUMBER') {
      attrValue.value = value;
    }

    if (type === 'ENUM') {
      value.forEach(val => {
        const enumValue = attrValue.value.find(v => v.key === val.key);
        if (enumValue) {
          enumValue.label = val.label;
        } else {
          attrValue.value.push({
            key: val.key,
            label: val.label,
          });
        }
      });
    }

    if (type === 'NESTED') {
      // TODO
    }

    if (type === 'SET') {
      value.forEach(val => {
        const setValue = attrValue.value.indexOf(val.old);
        if (setValue >= 0) {
          attrValue.value[setValue] = val.new;
        } else {
          attrValue.value.push(val.new);
        }
      });
    }

    await attrValue.save();
    return attrValue;
  } catch (error) {
    throw error;
  }
};

export const deleteAttributeValueById = async id => {
  await BaseValueModel.deleteOne({ _id: id });
  return true;
};

export const deleteAttributeById = async id => {
  const attribute = await findAttributeById(id);
  await deleteAttributeValueById(attribute.value);
  await attribute.remove();
  return true;
};
