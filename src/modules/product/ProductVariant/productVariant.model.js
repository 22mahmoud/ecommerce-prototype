import mongoose from 'mongoose';

const productVariantSchema = new mongoose.Schema(
  {
    sku: {
      type: String,
      unique: true,
    },
    product: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product',
    },
    attributes: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Attribute',
      },
    ],
  },
  { timestamps: true },
);

export const ProductVariantModel = mongoose.model(
  'ProductVariant',
  productVariantSchema,
);
