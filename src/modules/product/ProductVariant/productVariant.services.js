import * as Yup from 'yup';
import { isValidMongoObjectID } from '../../../utils/isValidMongoObjectId';
import { ProductVariantModel } from './productVariant.model';

export const insertProductVariant = async info => {
  const schema = Yup.object().shape({
    productID: Yup.mixed()
      .test('productID', 'product is not ObjectID', v => isValidMongoObjectID(v))
      .required(),
    sku: Yup.mixed(),
    attributes: Yup.array().of(
      Yup.mixed().test('productID', 'product is not ObjectID', v => isValidMongoObjectID(v)),
    ),
  });

  try {
    await schema.validate(info);
    const productVariant = await ProductVariantModel.create({
      product: info.productID,
      sku: info.sku,
      attributes: info.attributes,
    });
    return productVariant;
  } catch (error) {
    throw error;
  }
};

export const findProductVariantById = async id => {
  const schema = Yup.mixed()
    .test('productID', 'product is not ObjectID', v => isValidMongoObjectID(v))
    .required();
  try {
    await schema.validate(id);
    const productVariant = await ProductVariantModel.findById(id);
    if (!productVariant) {
      throw new Error('product not found');
    }

    return productVariant;
  } catch (error) {
    throw error;
  }
};

export const findProductVariations = async productID => {
  const schema = Yup.mixed()
    .test('productID', 'product is not ObjectID', v => isValidMongoObjectID(v))
    .required();
  try {
    await schema.validate(productID);
    const productVariations = await ProductVariantModel.where({
      product: productID,
    });

    return productVariations;
  } catch (error) {
    throw error;
  }
};
