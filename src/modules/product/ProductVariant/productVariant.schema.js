import { gql } from 'apollo-server-express';

export default gql`
  # import { Attribue, AttributeInput } from '../attribue'

  type ProductVariant {
    id: ID!
    sku: String
    attributes: [Attribute!]!
  }

  type Query {
    productVariants(productID: ID!): [ProductVariant!]!
  }

  type Mutation {
    createProductVariant(
      sku: String
      productID: ID!
      attributes: [AttributeInput]
    ): ProductVariant!
  }
`;
