import { productVariantService } from '.';
import { attributeService } from '../../attribute';
import { productService } from '..';
import { requireAuth } from '../../../utils/permissions';

export default {
  ProductVariant: {
    async attributes(root) {
      const attributes = await attributeService.findManyAttributesById(
        root.attributes,
      );
      return attributes.map(({ name, value }) => ({
        name,
        type: value.__type,
        value: () => {
          const { __type } = value;
          if (__type === 'ENUM') return value.value.map(({ key, label }) => ({ key, label }));
          return value.value;
        },
      }));
    },
  },

  Query: {
    async productVariants(_, { productID }) {
      await productService.findProductById(productID);
      const productVariants = await productVariantService.findProductVariations(
        productID,
      );
      return productVariants;
    },
  },

  Mutation: {
    createProductVariant: requireAuth(
      async (_, { sku, productID, attributes }, { user }) => {
        try {
          const product = await productService.findProductById(productID);
          if (String(product.owner) !== String(user._id)) {
            throw new Error('You must to be the owner of this product');
          }
          const attrs = await attributeService.insertManyAttributes(attributes);
          const productVariant = await productVariantService.insertProductVariant(
            {
              sku,
              productID,
              attributes: attrs.map(attr => attr._id),
            },
          );
          return productVariant;
        } catch (error) {
          throw error;
        }
      },
    ),
  },
};
