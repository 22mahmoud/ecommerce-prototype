import * as Yup from 'yup';
import { ProductModel } from './product.model';
import { isValidMongoObjectID } from '../../utils/isValidMongoObjectId';
import { ProductVariantModel } from './ProductVariant';
import { AttributeModel, BaseValueModel } from '../attribute';

export const insertProduct = async info => {
  const schema = Yup.object().shape({
    title: Yup.string().required(),
    owner: Yup.mixed()
      .test('userID', 'userid is not ObjectID', v => isValidMongoObjectID(v))
      .required(),
  });

  try {
    await schema.validate(info);
    const product = await ProductModel.create({
      title: info.title,
      owner: info.owner,
    });
    return product;
  } catch (error) {
    throw error;
  }
};

export const getProducts = async ({ where = {} } = {}) => {
  try {
    const products = await ProductModel.where(where);
    return products;
  } catch (error) {
    throw error;
  }
};

export const findProductById = async id => {
  const schema = Yup.mixed()
    .test('productID', 'product is not ObjectID', v => isValidMongoObjectID(v))
    .required();
  try {
    await schema.validate(id);
    const product = await ProductModel.findById(id);
    if (!product) {
      throw new Error('product not found');
    }

    return product;
  } catch (error) {
    throw error;
  }
};

export const deleteProductById = async id => {
  const schema = Yup.mixed()
    .test('productID', 'product is not ObjectID', v => isValidMongoObjectID(v))
    .required();
  try {
    await schema.validate(id);
    const product = await findProductById(id);
    const productVariations = await ProductVariantModel.where({
      product: product._id,
    }).populate('attributes');

    const productVariationsIds = [];
    const productVariantAttributesIds = [];
    const attributesValueIds = [];

    productVariations.forEach(pv => {
      productVariationsIds.push(pv._id);
      pv.attributes.forEach(attr => {
        productVariantAttributesIds.push(attr._id);
        attributesValueIds.push(attr.value);
      });
    });

    await Promise.all([
      product.remove(),
      ProductVariantModel.deleteMany({ _id: { $in: productVariationsIds } }),
      AttributeModel.deleteMany({ _id: { $in: productVariantAttributesIds } }),
      BaseValueModel.deleteMany({ _id: { $in: attributesValueIds } }),
    ]);

    return true;
  } catch (error) {
    throw error;
  }
};
