import { gql } from 'apollo-server-express';

export default gql`
  # import ProductVariant from './productVariant'

  type Product {
    id: ID!
    title: String!
    productVariations: [ProductVariant!]!
  }

  type Query {
    products: [Product!]!
    userProducts(userId: ID!): [Product!]!
  }

  type Mutation {
    createProduct(title: String!): Product!
    updateProduct(title: String): Product!
    deleteProduct(id: ID!): Boolean!
  }
`;
