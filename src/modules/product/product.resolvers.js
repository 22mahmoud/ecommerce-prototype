import { productService } from '.';
import { ProductVariantModel } from './ProductVariant';
import { requireAuth } from '../../utils/permissions';

export default {
  Product: {
    async productVariations(root) {
      const productVariations = await ProductVariantModel.where({
        product: root._id,
      });

      return productVariations;
    },
  },

  Query: {
    async products() {
      const products = await productService.getProducts();
      return products;
    },
    async userProducts(_, { userId }) {
      const products = await productService.getProducts({
        where: { owner: userId },
      });
      return products;
    },
  },

  Mutation: {
    createProduct: requireAuth(async (_, { title }, { user }) => {
      const product = await productService.insertProduct({
        title,
        owner: user._id,
      });
      return product;
    }),

    deleteProduct: requireAuth(async (_, { id }, { user }) => {
      try {
        const product = await productService.findProductById(id);
        if (String(product.owner) !== String(user._id)) {
          throw new Error('You must to be the owner of this product');
        }

        await productService.deleteProductById(id);
        return true;
      } catch (error) {
        throw error;
      }
    }),
  },
};
