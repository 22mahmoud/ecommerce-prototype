import * as productService from './product.services';

export { default as productResolvers } from './product.resolvers';
export * from './product.model';
export { default as productSchema } from './product.schema';
export { productService };
